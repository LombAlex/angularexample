import { Component, OnInit, Input } from '@angular/core';
import { AppareilService } from '../services/appareil.service';

@Component({
  selector: 'app-appareil',
  templateUrl: './appareil.component.html',
  styleUrls: ['./appareil.component.scss']
})
export class AppareilComponent implements OnInit {

  @Input() appareilName: string;
  // appareilName: string = 'Machine à laver';

  @Input() appareilStatus: string = 'éteint';
  // appareilStatus: string = 'éteint';

  @Input() index: number;
  @Input() id: number;

  getColor() {
    if(this.appareilStatus === 'allumé') {
      return 'green';
    } else if(this.appareilStatus === 'éteint') {
      return 'red';
    }
  }

  constructor(private appareilService: AppareilService) { }

  ngOnInit(): void {
  }

  getStatus() {
    return this.appareilStatus;
  }

  onSwitch() {
	  if(this.appareilStatus === 'allumé') {
	    this.appareilService.switchOffOne(this.index);
	  } else if(this.appareilStatus === 'éteint') {
	    this.appareilService.switchOnOne(this.index);
	  }
	}

}
