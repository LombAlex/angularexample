import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-view',
  templateUrl: './post-list-view.component.html',
  styleUrls: ['./post-list-view.component.scss']
})
export class PostListViewComponent {

  constructor() { }

    //Les données ne reste pas car elle sont rechargé avec la page
    //Si on y met dans un service, celui-ci concerveras les changements (actuel)

    posts = [
    {
      title: "Mon premier post",  
      content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ultrices dolor augue, at pharetra urna ornare id. Morbi vitae velit et elit fermentum rutrum et et sem. Sed euismod laoreet ligula. Maecenas feugiat urna in urna sodales, sed luctus est mollis. Ut vel massa ornare, fringilla ipsum a, scelerisque mi. Nulla neque risus, pulvinar venenatis sodales at, malesuada placerat libero. Etiam malesuada, nunc eu porttitor posuere, mauris nisi ullamcorper tellus, scelerisque sagittis est augue nec erat. Mauris id viverra eros. Vestibulum quis interdum nibh. Cras enim dui, fringilla lacinia nibh sed, imperdiet sodales erat. Mauris in ultrices ante. ",  
      loveIts: 20,  
      created_at: new Date("2001-01-01")
    },
    {
      title: "Aya un deuxième",  
      content: "Etiam porta at mi sed sollicitudin. Duis fringilla enim eu ipsum blandit, sit amet condimentum lacus tristique. Etiam non massa elit. Integer eros nulla, volutpat a rhoncus at, scelerisque at orci. Aenean sollicitudin purus porttitor libero molestie, nec euismod velit efficitur. Suspendisse id maximus justo. Maecenas sit amet egestas odio. Quisque enim est, porta quis laoreet ac, pharetra in augue. Suspendisse potenti. Sed scelerisque dolor in finibus lobortis. Cras commodo convallis porta. Donec sit amet tincidunt tellus. ",  
      loveIts: -20,  
      created_at: new Date("2002-02-02")
    },
    {
      title: "ça m'a saoulé c'est le dernier",  
      content: "Donec iaculis lacus eu vestibulum cursus. Curabitur venenatis neque vel dui sollicitudin, in aliquet erat tristique. Morbi vulputate molestie mi, ut mollis neque fermentum sit amet. Integer aliquam sit amet lectus et finibus. Phasellus vel lorem id purus fermentum sollicitudin tincidunt at sapien. Curabitur pulvinar turpis dui, at blandit nulla malesuada id. Pellentesque ultrices nulla eu enim molestie venenatis. In vulputate neque in pharetra fringilla. Maecenas vehicula vulputate nisi nec dapibus. Nunc placerat imperdiet velit, ac tincidunt nisl finibus ac. Vivamus convallis nec leo non feugiat. Sed facilisis varius dolor, elementum efficitur elit suscipit quis. ",  
      loveIts: 0,  
      created_at: Date.now()
    }
  ];

}
