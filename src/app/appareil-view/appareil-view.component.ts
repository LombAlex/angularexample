import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppareilService } from '../services/appareil.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-appareil-view',
  templateUrl: './appareil-view.component.html',
  styleUrls: ['./appareil-view.component.scss']
})
export class AppareilViewComponent implements OnInit, OnDestroy {

  title = 'mon-projet-angular';

  isAuth = false;

  //lastUpdate = new Date();
  lastUpdate = new Promise((resolve, reject) => {
    const date = new Date();
    setTimeout(
      () => {
        resolve(date);
      }, 2000
    );
  });

  // appareilOne = 'Machine à laver';
  // appareilTwo = 'Frigo';
  appareilThree = 'PS4';

  appareils: any[];
  appareilSubscription: Subscription;
  // [
  //   {
  //     name: 'Machine à laver',
  //     status: 'éteint'
  //   },
  //   {
  //     name: 'Frigo',
  //     status: 'allumé'
  //   },
  //   {
  //     name: 'Ordinateur',
  //     status: 'éteint'
  //   }
  // ];

  

  // constructor() {
  //   setTimeout(
  //     () => {
  //       this.isAuth = true;
  //     }, 4000
  //   );
  // }


  ngOnInit() {
    this.appareilSubscription = this.appareilService.appareilsSubject.subscribe(
      (appareils: any[]) => {
        this.appareils = appareils;
      }
    );
    this.appareilService.emitAppareilSubject();
  }

  ngOnDestroy() {
    this.appareilSubscription.unsubscribe();
  }

  constructor(private appareilService: AppareilService) {
    setTimeout(
      () => {
        this.isAuth = true;
      }, 4000
    );
  }

	onAllumer() {
	    this.appareilService.switchOnAll();
	}

	onEteindre() {
	    if(confirm('Etes-vous sûr de vouloir éteindre tous vos appareils ?')) {
	      this.appareilService.switchOffAll();
	    } else {
	      return null;
	    }
	}



}
