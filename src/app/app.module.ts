import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MonPremierComponent } from './mon-premier/mon-premier.component';
import { AppareilComponent } from './appareil/appareil.component';

//FormsModule
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//Services
import { AppareilService } from './services/appareil.service';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth-guard.service';
import { UserService } from './services/user.service';

//Route
import { Routes, RouterModule } from '@angular/router';

//Composant
import { PostListComponentComponent } from './post-list-component/post-list-component.component';
import { AuthComponent } from './auth/auth.component';
import { UserListComponent } from './user-list/user-list.component';
import { EditAppareilComponent } from './edit-appareil/edit-appareil.component';
import { AppareilViewComponent } from './appareil-view/appareil-view.component';
import { PostListViewComponent } from './post-list-view/post-list-view.component';
import { SingleAppareilComponent } from './single-appareil/single-appareil.component';
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';
import { NewUserComponent } from './new-user/new-user.component';





const appRoutes: Routes = [
  { path: 'appareils', canActivate: [AuthGuard], component: AppareilViewComponent },
  { path: 'appareils/:id', canActivate: [AuthGuard], component: SingleAppareilComponent },
  { path: 'auth', component: AuthComponent },
  { path: 'post-list', component: PostListViewComponent },
  { path: 'edit', component: EditAppareilComponent },
  { path: 'users', component: UserListComponent },
  { path: 'new-user', component: NewUserComponent },
  { path: '', component: AppareilViewComponent },

  { path: 'not-found', component: FourOhFourComponent },
  { path: '**', redirectTo: 'not-found' } //Redirection
];




@NgModule({
  declarations: [
    AppComponent,
    MonPremierComponent,
    AppareilComponent,
    PostListComponentComponent,
    AuthComponent,
    AppareilViewComponent,
    PostListViewComponent,
    SingleAppareilComponent,
    FourOhFourComponent,
    EditAppareilComponent,
    UserListComponent,
    NewUserComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,                //FormsModule !
    ReactiveFormsModule,        //Forms Reactive
    AppRoutingModule,
    RouterModule.forRoot(appRoutes) //Pour route !
  ],
  providers: [
    AppareilService,          //Services
    AuthService,
    AuthGuard, 
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
