import { Component, OnInit, OnDestroy } from '@angular/core';
// Ajouter OnInit

import { Observable, Subject, interval, Subscription } from 'rxjs';
//Pour créer Observable


//Service
import { AppareilService } from './services/appareil.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  secondes: number;
  counterSubscription: Subscription;

  constructor() { }           //

  ngOnInit() {
    const counter = interval(1000);
    this.counterSubscription = counter.subscribe(
      (value) => {
        this.secondes = value;
      },
      (error) => {
        console.log('Erreur avec le compteur : ' + error);
      },
      () => {
        console.log('Observable finis');
      }
    );
  }

  ngOnDestroy() {
    this.counterSubscription.unsubscribe();
  }
}









