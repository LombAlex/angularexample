import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post-list-component',
  templateUrl: './post-list-component.component.html',
  styleUrls: ['./post-list-component.component.scss']
})
export class PostListComponentComponent {

	@Input() nom: string;
	@Input() contenue: string;
	@Input() nbJaime: number;
	@Input() date: Date;

	findColor() {
		if(this.nbJaime < 0)
			return "red";
		else if (this.nbJaime > 0)
			return "green";
		else
			return "black";
	}

	jAime() {
		this.nbJaime++;
	}
	jAimePas() {
		this.nbJaime--;
	}

	constructor() { }
}
